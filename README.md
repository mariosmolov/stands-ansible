# Описание

Стенд для практики к уроку «Автоматизация администрирования. Ansible.»  

Разворачивается два сервера: `host1` и `host2`. При развертывании Vagrant запускается Ansible [playbook](provisioning/playbook.yml). 

# Инструкция по применению
## Перед запуском

## Запускаем и работаем со стендом

Поднимем виртуальные машины: `vagrant up`
Узнаем порты виртуальных машин `vagrant ssh-config`
Отредактируем, если необходимо, порты в файле staging/hosts

Запустим роль: `ansible-playbook nginx.yml`  

## Проверка

Делаем `curl http://192.168.11.150:8080` и `curl http://192.168.11.151:8080`